package mensaupb.services

import com.google.inject.Inject
import mensaupb.models.Restaurant
import mensaupb.services.upstream.JoinedApiService
import play.api.Configuration
import play.api.cache.AsyncCacheApi

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class RestaurantService @Inject()(cache: AsyncCacheApi,
                                  joinedApiService: JoinedApiService,
                                  config: Configuration)
                                 (implicit exec: ExecutionContext) {

  val CACHE_DURATION = config.getOptional[Int]("stw.api_cache_duration_in_seconds")
    .getOrElse(0)
    .seconds

  def findAll(): Future[Iterable[Restaurant]] =
    cachedRestaurants

  def find(key: String): Future[Option[Restaurant]] =
    cachedRestaurants.map { restaurants =>
      restaurants
        .filter(_.key == key)
        .headOption
    }

  private def cachedRestaurants =
    cache.getOrElseUpdate[Iterable[Restaurant]]("restaurants", CACHE_DURATION) {
      joinedApiService.requestRestaurants
    }
}
