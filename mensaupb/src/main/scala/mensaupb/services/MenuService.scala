package mensaupb.services

import com.google.inject.Inject
import mensaupb.models.{ExtendedMenuFixer, ExtendedStwMenu}
import mensaupb.services.upstream.JoinedApiService
import play.api.Configuration
import play.api.cache.AsyncCacheApi

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}


class MenuService @Inject()(cache: AsyncCacheApi,
                            joinedApiService: JoinedApiService,
                            config: Configuration)
                           (implicit exec: ExecutionContext) {

  val CACHE_DURATION = config.getOptional[Int]("stw.api_cache_duration_in_seconds")
    .getOrElse(0)
    .seconds

  def findWithUpdateMenus(restaurant: Option[String], date: Option[String]) = {
    val fMenus = find(restaurant, date)

    val withUpdateMenus = fMenus.map { menus =>
      val distinctDates = menus.map(_.date).distinct.toSet
      val distinctRestaurants = menus.map(_.restaurant).distinct.toSet

      val mDR = (d: String, r: String) => ExtendedStwMenu(Array.empty[String], Array.empty[String], "Update available", "Update verfügbar", "Update available", d, "Description DE.", "Description EN.", "", "", "Bitte updaten", "Please update", None, 0, 0.0, 0.0, 0.0, "", r, "", "")

      val dr =
        distinctRestaurants
          .flatMap(r => distinctDates.map(d => (r, d)))
          .map { case (r, d) =>
            mDR(r, d)
          }


      // add "please update" at all places!

      menus ++ dr
    }.map(sortMenus)

    withUpdateMenus
  }

  def find(restaurant: Option[String], date: Option[String]) = {
    cache.getOrElseUpdate[Array[ExtendedStwMenu]](cacheKey(restaurant, date), CACHE_DURATION) {
      for {
        menus <- joinedApiService.requestMenus(restaurant, date)
        // TODO: übertragen!
        fixedMenus = menus.map(ExtendedMenuFixer.fix)
        sortedMenus = sortMenus(fixedMenus)
      } yield sortedMenus
    }
  }

  private def sortMenus(fixedMenus: Array[ExtendedStwMenu]) = {
    fixedMenus
      .sortBy(_.name_de)
      .sortBy(_.restaurant)
      .sortBy(_.date)
  }

  def find(key: String): Future[Option[ExtendedStwMenu]] = {
    find(key).map { menus =>
      menus.find(_.key == key)
    }
  }

  def find(restaurant: String, date: String, name_en: String): Future[Option[ExtendedStwMenu]] = {
    find(Some(restaurant), Some(date)).map { menus =>
      menus.find(_.name_en == name_en)
    }
  }

  private def cacheKey(restaurant: Option[String], date: Option[String]) = s"menus.$restaurant.$date"

  private def cacheKey(menuKey: String) = s"menu.$menuKey"
}
