package mensaupb.services.upstream

object BaseUri{
    def get() :String = sys.env.get("STW_URL").getOrElse("http://www.studentenwerk-pb.de")
}  
