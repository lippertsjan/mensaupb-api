package mensaupb.services.upstream

import com.google.inject.Inject
import mensaupb.models.ExtendedStwMenu

import scala.concurrent.ExecutionContext

class JoinedApiService @Inject()(stwService: StwApiService,
                                 menuExtractorService: MenuExtractorService)
                                (implicit exec: ExecutionContext) {

  def requestMenus(restaurant: Option[String], date: Option[String]) = {
    (for {
      apiMenus <- stwService.requestMenus(restaurant, date)
      crawledMenus <- menuExtractorService.crawl(restaurant, date)
    } yield (apiMenus, crawledMenus))
      .map { case (apiMenus, crawledMenus) =>
        apiMenus.map { menu =>
          val matchedMenu = crawledMenus.find(_.name == menu.name_de)
          ExtendedStwMenu.from(menu, matchedMenu.flatMap(_.nutritionals))
        }
      }
  }

  def requestRestaurants() = {
    stwService.requestRestaurants
  }

}
