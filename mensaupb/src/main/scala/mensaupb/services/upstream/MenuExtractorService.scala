package mensaupb.services.upstream

import com.google.inject.Inject
import mensaupb.models.NutritionalInfo
import mensaupb.models.Menu
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import play.api.Configuration
import play.api.cache.AsyncCacheApi
import play.api.libs.ws.{WSClient, WSResponse}
import utils.RestaurantKeyMapper

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scala.concurrent.duration._

/**
  * @todo Extract exact ingredients
  * @todo Extract prices per class
  * @todo Add "exceptional stuff" handling
  */
protected class MenuExtractorService @Inject()(ws: WSClient,
                                     cache: AsyncCacheApi,
                                     config: Configuration)
                                    (implicit exec: ExecutionContext) {

  private val strongReplacementString = "<strong>.*:?</strong> ?"

  val CACHE_DURATION =
    config.getOptional[Int]("stw.crawler_cache_duration_in_seconds")
      .getOrElse(0)
      .seconds

  def crawl(restaurant: Option[String], date: Option[String]): Future[List[Menu]] = {
    val cacheKey = s"crawled.$restaurant.$date"

    cache.getOrElseUpdate[List[Menu]](cacheKey, CACHE_DURATION) {
      (restaurant, date) match {
        case (Some(r), Some(d)) => tryCrawl(r, d)
        case _ => Future.successful(List[Menu]())
      }
    }
  }

  private def tryCrawl(r: String, d: String) = {
    val restaurant = RestaurantKeyMapper.fromApiKeysToWebsite(r)
    ws.url("http://www.studierendenwerk-pb.de/gastronomie/speiseplaene/" + restaurant + "/?tx_pamensa_mensa%5Bdate%5D=" + d)
      .withRequestTimeout(10000.millis)
      .get().map { response =>
      parse(response)
    }
  }

  private def parse(response: WSResponse): List[Menu] = {
    val browser = JsoupBrowser()
    val doc = browser.parseString(response.body)
    val menusAsJsoup = doc >> elementList(".table-dishes .description")
    val menus = menusAsJsoup.map(extractMenuBaseData)

    val ingsAsJsoup = doc >> elementList(".ingredients-list")
    val ings = ingsAsJsoup.map(extractAdditionalStuff)

    val both = menus.zip(ings)
      .map { case ((name: String, img: Option[String], prices: List[String]), (ingredients: Option[List[String]], nutritions: Option[NutritionalInfo])) =>
        Menu(name, img, prices, ingredients, nutritions)
      }
    both
  }

  private def extractMenuBaseData(m: Element) = {
    val name = (m >> elementList("h4")).head.innerHtml
    val img = (m >> elementList("img")).headOption.map(e => BaseUri.get() + e.attr("src"))
    val prices = (m >> elementList(".price")).map(_.innerHtml.replaceAll(strongReplacementString, "").replaceAll(" ?€", "").replaceAll(",", "."))

    (name, img, prices)
  }


  private def extractAdditionalStuff(m: Element) = {
    val in =
      (m >> elementList(".ingredients p"))
        .headOption
        .map { e =>
          e.innerHtml
            .replaceAll(strongReplacementString, "")
            .split("<br> ?")
            .toList
        }
    (in, extractNutritionalInformation(m))
  }


  private def extractNutritionalInformation(m: Element): Option[NutritionalInfo] = {
    val valueMap = (m >> elementList(".nutritions p"))
      .headOption
      .map { e =>
        e.innerHtml
          .replaceAll(strongReplacementString, "")
          .split("<br> ?")
          .toList
          .map { str =>
            val strings = str.split(" ?= ?")
            Try(strings(0) -> strings(1))
          }
          .filter(_.isSuccess)
          .map(_.get)
          .toMap
      }
    valueMap.flatMap { m1 =>
      Try(NutritionalInfo.from(m1("Brennwert"), m1("Fett"), m1("Kohlenhydrate"), m1("Eiweiß"))).toOption
    }
  }
}
