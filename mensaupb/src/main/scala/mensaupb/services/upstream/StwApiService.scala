package mensaupb.services.upstream

import javax.inject.Inject
import mensaupb.models.{Restaurant, StwMenu, StwRestaurant}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess}
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Success, Try}

class StwApiService @Inject()(ws: WSClient)
                             (implicit exec: ExecutionContext) {
  def requestRestaurants: Future[Iterable[Restaurant]] = {
    val secret = sys.env.get("STW_SECRET")
    if (secret.isEmpty) {
      return Future.failed(new Exception("Env not setup"))
    }

    return doRestaurantsRequest(secret.get)
          .map(parseRestaurantsResponse)
      
  }

  private def doRestaurantsRequest(secret: String) = {
    val uri = getBaseUri() + "/fileadmin/shareddata/access2.access2.php?id=${secret}&outputformat=json&getrestaurants=1"
    ws.url(uri)
      .withHttpHeaders("Accept" -> "application/json")
      .withRequestTimeout(10000.millis)
      .get()
  }

  private def parseRestaurantsResponse(response: WSResponse) = {
    response
      .json
      .validate[Map[String, StwRestaurant]] match {
      case s: JsSuccess[Map[String, StwRestaurant]] => s.get.map { case (k, r) => Restaurant.from(k, r) }
      case e: JsError => throw new Exception("Json Errors: " + e.errors.mkString(", "))
    }
  }

  def requestMenus(restaurant: Option[String], date: Option[String]): Future[Array[StwMenu]] =
    Try(sys.env("STW_SECRET")) match {
      case Success(secret) if secret.nonEmpty =>
        doMenusRequest(restaurant, date, secret)
          .map(parseMenusResponse)
      case _ => Future.failed(new Exception("Env not setup"))
    }

  private def doMenusRequest(restaurant: Option[String], date: Option[String], secret: String): Future[WSResponse] = {
    var uri = getBaseUri()+s"/fileadmin/shareddata/access2.php?id=${secret}&outputformat=json"


    if (restaurant.isDefined) {
      uri += s"&restaurant=${restaurant.get}"
    }
    if (date.isDefined) {
      uri += s"&date=${date.get}"
    }


    Logger.debug(s"Request: $uri")

    ws.url(uri)
        .withHttpHeaders("Accept" -> "application/json")
        .withRequestTimeout(10000.millis)
        .get()
  }

  private def parseMenusResponse(response: WSResponse) = {
    response
      .json
      .validate[Array[StwMenu]] match {
      case s: JsSuccess[Array[StwMenu]] =>
        s.get
      case e: JsError =>
        throw new Exception("Json Errors: " + e.errors.mkString(", "))
    }
  }

  private def getBaseUri(): String = { return BaseUri.get(); }
}
