package mensaupb.models

import play.api.libs.json.{Json, OWrites, Reads}

import scala.util.Try

case class NutritionalInfo(kj: Option[Double],
                           kcal: Option[Double],
                           fat: Double,
                           carbohydrates: Double,
                           protein: Double)

object NutritionalInfo {
  def from(caloricValue: String,
            fat: String,
            carbohydrates: String,
            protein: String): NutritionalInfo = {
    val kjAndkcal = caloricValue.extractKjAndKcal

    NutritionalInfo(
      kjAndkcal.map(_._1),
      kjAndkcal.map(_._2),
      fat.grammValueToDouble,
      carbohydrates.grammValueToDouble,
      protein.grammValueToDouble)
  }
  implicit val NutritionalInfoWrites: OWrites[NutritionalInfo] = Json.writes[NutritionalInfo]
  implicit val NutritionalInfoReads: Reads[NutritionalInfo] = Json.reads[NutritionalInfo]

  
  implicit class StringExtension(s: String){
    def grammValueToDouble: Double = s.replaceAll(" ?g", "").replaceAll(",", ".").toDouble
    def extractKjAndKcal: Option[(Double, Double)] = {
      val sp = s.replaceAll("(\\(| ?kcal|\\))", "").split(" ?kJ ?")
      Try((sp(0).toDouble, sp(1).toDouble)).toOption
    }
  }
}
