package mensaupb.models

import play.api.libs.json.{Json, OWrites, Reads}

case class ExtendedStwMenu(allergens: Array[String],
                           badges: Array[String],
                           category: String,
                           category_de: String,
                           category_en: String,
                           date: String,
                           description_de: String,
                           description_en: String,
                           image: String,
                           key: String,
                           name_de: String,
                           name_en: String,
                           nutritionalInfo: Option[NutritionalInfo],
                           order_info: Int,
                           priceGuests: Double,
                           priceStudents: Double,
                           priceWorkers: Double,
                           pricetype: String,
                           restaurant: String,
                           subcategory_de: String,
                           subcategory_en: String) {

}


object ExtendedStwMenu {
  implicit val StwMenuWrites: OWrites[ExtendedStwMenu] = Json.writes[ExtendedStwMenu]
  implicit val StwMenuReads: Reads[ExtendedStwMenu] = Json.reads[ExtendedStwMenu]

  def from(stwMenu: StwMenu,
           nutritionalInfo: Option[NutritionalInfo]
          ): ExtendedStwMenu =
    new ExtendedStwMenu(stwMenu.allergens, stwMenu.badges, stwMenu.category, stwMenu.category_de, stwMenu.category_en, stwMenu.date, stwMenu.description_de, stwMenu.description_en, stwMenu.image, stwMenu.key, stwMenu.name_de, stwMenu.name_en, nutritionalInfo, stwMenu.order_info, stwMenu.priceGuests, stwMenu.priceStudents, stwMenu.priceWorkers, stwMenu.pricetype, stwMenu.restaurant, stwMenu.subcategory_de, stwMenu.subcategory_en)

}
