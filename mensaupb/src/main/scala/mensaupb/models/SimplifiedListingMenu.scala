package mensaupb.models

import play.api.libs.json.{Json, OWrites, Reads}

case class SimplifiedListingMenu(date: String,
                                 name_de: String,
                                 name_en: String,
                                 category: String,
                                 category_de: String,
                                 category_en: String,
                                 priceStudents: Double,
                                 badges: Array[String],
                                 restaurant: String,
                                 pricetype: String,
                                 image: String,
                                 key: String)

object SimplifiedListingMenu {

  def from(m: ExtendedStwMenu) = SimplifiedListingMenu(m.date,
    m.name_de,
    m.name_en,
    m.category,
    m.category_de,
    m.category_en,
    m.priceStudents,
    m.badges,
    m.restaurant,
    m.pricetype,
    m.image,
    m.key)

    implicit val SimplifiedMenuWrites: OWrites[SimplifiedListingMenu] = Json.writes[SimplifiedListingMenu]
    implicit val SimplifiedMenuReads: Reads[SimplifiedListingMenu] = Json.reads[SimplifiedListingMenu]


}