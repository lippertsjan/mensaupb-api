package mensaupb.models

import play.api.libs.json.Json

case class StwMenu(date: String,
                   name_de: String,
                   name_en: String,
                   description_de: String,
                   description_en: String,
                   category: String,
                   category_de: String,
                   category_en: String,
                   subcategory_de: String,
                   subcategory_en: String,
                   priceStudents: Double,
                   priceWorkers: Double,
                   priceGuests: Double,
                   allergens: Array[String],
                   order_info: Int,
                   badges: Array[String],
                   restaurant: String,
                   pricetype: String,
                   image: String
                  )

object StwMenu{
  implicit val StwMenuWrites = Json.writes[StwMenu]
  implicit val StwMenuReads = Json.reads[StwMenu]

  implicit class StwMenuExtensions(stwMenu: StwMenu) {
    def key = s"${stwMenu.date}_${stwMenu.restaurant}_${stwMenu.order_info}"
  }
}