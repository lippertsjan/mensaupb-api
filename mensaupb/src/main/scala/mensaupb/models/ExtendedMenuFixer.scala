package mensaupb.models

object ExtendedMenuFixer {

  val restaurantDefaultDishDenoter = Set("cafete", "grill-cafe")

  def fix(menu: ExtendedStwMenu): ExtendedStwMenu = {
    if (menu.priceStudents < 1.0) {
      return menu.copy(category = "sidedish", category_de = "Beilagen", category_en = "Side Dish")
    }

    if (!menu.category.isEmpty) menu
    else copyWithDish(menu)
  }

  private def copyWithDish(menu: ExtendedStwMenu) = {
    menu.copy(category = "dish", category_de = "Essen", category_en = "Dish")
  }
}
