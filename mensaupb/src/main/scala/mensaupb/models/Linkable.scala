package mensaupb.models

trait Linkable {
  val link: String
}
