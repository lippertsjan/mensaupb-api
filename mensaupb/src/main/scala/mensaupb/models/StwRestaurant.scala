package mensaupb.models

import play.api.libs.json.{Json, OWrites, Reads}

case class StwRestaurant(name: String,
                      location: String,
                      active: Boolean)

object StwRestaurant {

  implicit val StwRestaurantWrites: OWrites[StwRestaurant] = Json.writes[StwRestaurant]
  implicit val StwRestaurantReads: Reads[StwRestaurant] = Json.reads[StwRestaurant]

}