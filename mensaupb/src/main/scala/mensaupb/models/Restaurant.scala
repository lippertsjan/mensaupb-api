package mensaupb.models

import play.api.libs.json.{Json, OWrites, Reads}
import play.api.routing.Router

case class Restaurant(key: String,
                      name: String,
                      location: String,
                      active: Boolean)

case class LinkableRestaurant(key: String,
                              name: String,
                              location: String,
                              active: Boolean,
                              link: String,
                              menus: String) extends Linkable
object LinkableRestaurant{
  implicit val LinkableRestaurantWrites: OWrites[LinkableRestaurant] = Json.writes[LinkableRestaurant]
}
object Restaurant {

  implicit val RestaurantWrites: OWrites[Restaurant] = Json.writes[Restaurant]
  implicit val RestaurantReads: Reads[Restaurant] = Json.reads[Restaurant]

  def from(key: String, stwRestaurant: StwRestaurant) =
    Restaurant(key,
      stwRestaurant.name,
      stwRestaurant.location,
      stwRestaurant.active)

  def linkable(r: Restaurant, baseUrl: String) =
    LinkableRestaurant(r.key,
      r.name,
      r.location,
      r.active,
      s"$baseUrl/${r.key}".replace("//","/"),
      s"$baseUrl/${r.key}/menus".replace("//","/"),
    )
}