package mensaupb.models

/**
  * @todo ingredients and nutritionals need to split into exact "enums"
  */
case class Menu(name: String,
                img: Option[String],
                prices: List[String],
                ingredients: Option[List[String]],
                nutritionals: Option[NutritionalInfo])
