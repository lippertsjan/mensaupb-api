package mensaupb.models

object ExtendedMenuValidator {
  def isValid(m:ExtendedStwMenu ): Boolean = {
    Set(m.category, m.date, m.name_de, m.name_en)
      .map(!_.isEmpty)
      .reduce(_ && _)
  }

}
