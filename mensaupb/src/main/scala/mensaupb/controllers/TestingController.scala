package mensaupb.controllers

import com.google.inject.Inject
import mensaupb.services.upstream.JoinedApiService
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, InjectedController}

import scala.concurrent.ExecutionContext


class TestingController @Inject()(joinedApiService: JoinedApiService)
                                 (implicit exec: ExecutionContext)
  extends InjectedController {


  def order_info_unique_on_day(): Action[AnyContent] = Action.async {
    joinedApiService.requestMenus(None, None)
      .map { case menus =>
        menus.map { m => (s"${m.restaurant}-${m.date}", m.order_info) }
          .groupBy(_._1)
          .map { case (k, vs) =>
            val os = vs.map(_._2)
            (k, os.distinct.size == os.size)
          }.filterNot(_._2)
      }.map { m =>
      Ok(Json.toJson(m))
    }
  }
}