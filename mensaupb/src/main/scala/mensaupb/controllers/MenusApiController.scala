package mensaupb.controllers

import com.google.inject.Inject
import mensaupb.models.{ExtendedStwMenu, SimplifiedListingMenu}
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, InjectedController}
import mensaupb.services.MenuService
import utils.Log

import scala.concurrent.ExecutionContext

class MenusApiController @Inject()(menuService: MenuService)
                                     (implicit exec: ExecutionContext)
  extends InjectedController {

  val CACHE_AGE_12_HOURS = 12*60*60

  def menus(restaurant: Option[String], date: Option[String]): Action[AnyContent] = Action.async {
    menuService.find(restaurant, date)
      .map { ms =>
        Ok(Json.toJson(ms)).withHeaders(CACHE_CONTROL -> s"max-age=$CACHE_AGE_12_HOURS")
      }
      .recover {
        case e: IllegalArgumentException => BadRequest(e.getMessage)
        case e =>
          Log.hideAndLog(e)
          InternalServerError
      }
  }

  def withUpdateMenus(restaurant: Option[String], date: Option[String]): Action[AnyContent] = Action.async {
    menuService.findWithUpdateMenus(restaurant, date)
      .map { ms =>
        Ok(Json.toJson(ms)).withHeaders(CACHE_CONTROL -> s"max-age=$CACHE_AGE_12_HOURS")
      }
      .recover {
        case e: IllegalArgumentException => BadRequest(e.getMessage)
        case e =>
          Log.hideAndLog(e)
          InternalServerError
      }
  }

  def simplifiedListing(restaurant: Option[String],
                        date: Option[String]): Action[AnyContent] = Action.async {
    menuService.find(restaurant, date)
      .map {menus =>
        menus.map(SimplifiedListingMenu.from)
      }
      .map { ms =>
        Ok(Json.toJson(ms)).withHeaders(CACHE_CONTROL -> s"max-age=$CACHE_AGE_12_HOURS")
      }
      .recover {
        case e: IllegalArgumentException => BadRequest(e.getMessage)
        case e =>
          Log.hideAndLog(e)
          InternalServerError
      }
  }

  def menu(key: String) = Action.async{
    menuService.find(key)
      .map{
        case None => NotFound
        case Some(m) => Ok(Json.toJson(m))
      }
      .recover{
        case e: IllegalArgumentException => BadRequest(e.getMessage)
        case e =>
          Log.hideAndLog(e)
          InternalServerError
      }
  }

  def menuImage(key: String) = Action.async {
    menuService.find(key)
      .map{
        case None => NotFound
        case Some(m) => Redirect(m.image)
      }
      .recover{
        case e: IllegalArgumentException => BadRequest(e.getMessage)
        case e =>
          Log.hideAndLog(e)
          InternalServerError
      }
  }


  def restaurantDate(restaurant: String, date: String) = Action.async {
    menuService.find(Some(restaurant), Some(date))
      .map { ms =>
        Ok(Json.toJson(ms))
      }
      .recover {
        case e: IllegalArgumentException => BadRequest(e.getMessage)
        case e => Log.hideAndLog(e)
          InternalServerError
      }
  }
}
