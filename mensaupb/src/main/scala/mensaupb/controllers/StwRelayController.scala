package mensaupb.controllers

import com.google.inject.Inject
import mensaupb.services.upstream.StwApiService
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, InjectedController}
import utils.Log

import scala.concurrent.ExecutionContext


class StwRelayController @Inject()(stwService: StwApiService)
                                  (implicit exec: ExecutionContext)
  extends InjectedController {

  def menus(restaurant: Option[String], date: Option[String]): Action[AnyContent] = Action.async { implicit request =>
    stwService.requestMenus(restaurant, date).map { menus =>
      Ok(Json.toJson(menus))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }

  def restaurants: Action[AnyContent] = Action.async {
    stwService.requestRestaurants.map { restaurants =>
      Ok(Json.toJson(restaurants))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }

}
