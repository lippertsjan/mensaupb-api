package mensaupb.controllers

import com.google.inject.Inject
import mensaupb.models.Restaurant
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, InjectedController}
import mensaupb.services.RestaurantService
import utils.Log

import scala.concurrent.ExecutionContext

class RestaurantsApiController @Inject()(restaurantService: RestaurantService)
(implicit exec: ExecutionContext)
extends InjectedController {
  val CACHE_AGE_1_WEEK = 7*24*60*60

  def restaurants(): Action[AnyContent] = Action.async { implicit request =>
    restaurantService.findAll()
      .map{rs => rs.map(r => Restaurant.linkable(r, request.uri))}
      .map { rs =>
        Ok(Json.toJson(rs))
          .withHeaders(CACHE_CONTROL -> s"max-age=$CACHE_AGE_1_WEEK")
    }.recover {
      case e => 
        Log.hideAndLog(e)
        InternalServerError
      }
    }

    def restaurant(key: String): Action[AnyContent] = Action.async {
      restaurantService.find(key).map { 
        case Some(r) =>  Ok(Json.toJson(r))
        case None => NotFound
      }.recover {
          case e => 
            Log.hideAndLog(e)
            InternalServerError
          }
        }

      }
