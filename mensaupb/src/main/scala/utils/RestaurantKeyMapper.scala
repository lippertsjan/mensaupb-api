package utils

object RestaurantKeyMapper {
  val fromApiKeysToWebsite: Map[String, String] = Map(
    "bistro-hotspot" -> "bistro-hotspot",
    "cafete" -> "cafete",
    "campus-doener" -> "campus-doener",
    "grill-cafe" -> "grillcafe",
    "mensa-academica-paderborn" -> "mensa-academica",
    "mensa-forum-paderborn" -> "mensa-forum",
    "mensa-hamm" -> "mensa-basilica-hamm",
    "mensa-lippstadt" -> "mensa-atrium-lippstadt",
    "mensula" -> "mensula",
    "one-way-snack" -> "one-way-snack",
    "mensa-academica" -> "mensa-academica",
    "mensa-forum" -> "mensa-forum")

  val fromWebsiteToApiKeys: Map[String, String] = Map(
    "bistro-hotspot" -> "bistro-hotspot",
    "cafete" -> "cafete",
    "campus-doener" -> "campus-doener",
    "grillcafe" -> "grill-cafe",
    "mensa-academica" -> "mensa-academica-paderborn",
    "mensa-forum" -> "mensa-forum-paderborn",
    "mensa-basilica-hamm" -> "mensa-hamm",
    "mensa-atrium-lippstadt" -> "mensa-lippstadt",
    "mensula" -> "mensula",
    "one-way-snack" -> "one-way-snack",
    "mensa-academica-paderborn" -> "mensa-academica-paderborn",
    "mensa-forum-paderborn" -> "mensa-forum-paderborn")
}
