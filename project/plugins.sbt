// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.15")

// coverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0-M3")

// Swagger doc
addSbtPlugin("com.iheart" %% "sbt-play-swagger" % "0.7.4")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.9.0")
