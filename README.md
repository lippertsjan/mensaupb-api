API Collection
==============

Despite the name, this project contains all of lippertsjan's APIs (metal only, mensaupb).

[![pipeline status](https://gitlab.com/lippertsjan/mensaupb-api/badges/master/pipeline.svg)](https://gitlab.com/lippertsjan/mensaupb-api/commits/master)
[![coverage report](https://gitlab.com/lippertsjan/mensaupb-api/badges/master/coverage.svg)](https://gitlab.com/lippertsjan/mensaupb-api/commits/master)

The project source is hosted on [gitlab.com](https://gitlab.com/lippertsjan/mensaupb-api/).


Notes
=====

 * Uses https://github.com/ruippeixotog/scala-scraper
 * Play Scala
 * Wrapper around STW API for [MensaUPB](https://github.com/ironjan/MensaUPB)
   * Was necessary because the STW does not respond to API extension requests
   * Some functionality was requested for the app and is not provided by the STW API (e.g. nutrients)
 * swagger.json available at http://localhost:9000/assets/swagger.json, ui at http://localhost:9000/docs/swagger-ui/index.html
 * View logs `heroku addons:open logdna` 
