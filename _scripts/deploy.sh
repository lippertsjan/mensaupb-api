#!/bin/bash
if [ -z "$CI" ]; then
    echo "We're not running on CI... (or on an unsupported CI)"
    exit 1
fi

git remote add heroku $DEPLOYMENT_URL
git push heroku master:master
