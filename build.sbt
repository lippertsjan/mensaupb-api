name := """mensaupb-api"""

version := "1.0-SNAPSHOT"

lazy val root =
  (project in file("."))
    .enablePlugins(PlayScala, SwaggerPlugin)
    .aggregate(mensaupb, metalonly)
    .dependsOn(mensaupb, metalonly)

lazy val mensaupb = project
  .settings(
    name := "Mensa UPB API",
    libraryDependencies ++= commonDependencies,
  )


lazy val metalonly = project
.settings(
    name := "Metal Only API",
    libraryDependencies ++= commonDependencies,
  )

scalaVersion := "2.12.6"

lazy val commonDependencies = Seq(
  ehcache,
  guice,
  ws,

  "com.google.inject" % "guice" % "4.2.3",
  "com.typesafe.play" %% "cachecontrol" % "1.1.7",
  "com.typesafe.play" %% "play-json" % "2.9.4",
  "net.ruippeixotog" %% "scala-scraper" % "2.2.1",
  "org.webjars" % "swagger-ui" % "2.2.10",
)

swaggerDomainNameSpaces := Seq("models")
