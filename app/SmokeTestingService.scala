import com.google.inject.{Inject, Singleton}
import play.api.{Application, Logger}

import scala.util.{Success, Try}

@Singleton
class SmokeTestingService @Inject()(application: Application) {
  val mandatoryEnvironmentVariables = Set("STW_SECRET")


  Try(sys.env("STW_SECRET")) match {
      case Success(value) if value.nonEmpty => {/* STW_SECRET seems to be set up. */}
      case _ => {
        Logger.error("The environment variable STW_SECRET is not set up.")
        play.api.Play.stop(application)
      }
    }

}
