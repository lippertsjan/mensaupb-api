FROM hseeberger/scala-sbt:8u171_2.12.6_1.1.5
WORKDIR /usr/src
COPY build.sbt build.sbt
COPY project/plugins.sbt project/plugins.sbt
RUN sbt compile

COPY . .
RUN sbt dist

WORKDIR /usr/src/target/universal
RUN unzip mensaupb-api-1.0-SNAPSHOT.zip

WORKDIR /usr/src/target/universal/mensaupb-api-1.0-SNAPSHOT
COPY docker/insecure_application.conf ./conf/application.conf
CMD ./bin/mensaupb-api
