package utils

object Log {
  def hideAndLog(e: Throwable): Exception = {
    e.printStackTrace()
    new Exception("Internal Server Error")
  }
}
