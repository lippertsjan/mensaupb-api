package metalonly.models

import play.api.libs.json.Json

case class Wish(nick: String,
                artist: String,
                title: String,
                greeting: String)
object Wish{
  implicit val WishReads = Json.reads[Wish]
  implicit val WishWrites = Json.writes[Wish]
  implicit class WishExt(w: Wish) {
    def toMap: Map[String, String] = Map(
      "nick" -> w.nick,
      "artist" -> w.artist,
      "song" -> w.title,
      "greet" -> w.greeting)
  }
}