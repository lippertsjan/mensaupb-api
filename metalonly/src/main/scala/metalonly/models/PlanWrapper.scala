package metalonly.models

import play.api.libs.json.{Json, OWrites, Reads}

case class PlanWrapper(plan: Array[PlanEntry])

object PlanWrapper {
  implicit val PlanWrapperWrites: OWrites[PlanWrapper] = Json.writes[PlanWrapper]
  implicit val PlanWrapperReads: Reads[PlanWrapper] = Json.reads[PlanWrapper]
}