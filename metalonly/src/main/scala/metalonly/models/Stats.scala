package metalonly.models

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, OWrites, Reads}

case class Stats(showInformation: ShowInformation,
                 maxNoOfWishesReached: Boolean,
                 maxNoOfGreetingsReached: Boolean,
                 maxNoOfWishes: Int,
                 maxNoOfGreetings: Int,
                 track: Track) {
  val moderated = showInformation.moderated


}

object Stats {
  implicit val RawStatsWrites: OWrites[Stats] = Json.writes[Stats]
  //noinspection ConvertibleToMethodValue because of overload
  implicit val RawStatsReads: Reads[Stats] = (
    (JsPath \ "moderator").read[String] and
      (JsPath \ "show").read[String] and
      (JsPath \ "genre").read[String] and
      (JsPath \ "wunschvoll").read[String].map("1"==_) and
      (JsPath \ "grussvoll").read[String].map("1"==_) and
      (JsPath \ "wunschlimit").read[String].map(_.toInt) and
      (JsPath \ "grusslimit").read[String].map(_.toInt) and
      (JsPath \ "track").read[Track]
    ) (Stats.apply(_, _, _, _, _, _, _, _))


    def apply(moderator: String,
              show: String,
              genre: String,
              maxNoOfWishesReached: Boolean,
              maxNoOfGreetingsReached: Boolean,
              maxNoOfWishes: Int,
              maxNoOfGreetings: Int,
              track: Track): Stats = new Stats(ShowInformation(moderator, show, genre), maxNoOfWishesReached, maxNoOfGreetingsReached, maxNoOfWishes, maxNoOfGreetings, track)

}