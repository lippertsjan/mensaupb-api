package metalonly.models

import play.api.libs.json.{Json, OWrites, Reads}

case class Track(artist: String,
                 title: String)

object Track {
  implicit val TrackWrites: OWrites[Track] = Json.writes[Track]
  implicit val TrackReads: Reads[Track] = Json.reads[Track]
}