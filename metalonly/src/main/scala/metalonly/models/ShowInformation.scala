package metalonly.models

import play.api.libs.json.Json

case class ShowInformation(moderator: String,
                           show: String,
                           genre: String) {
  val moderated = "MetalHead" != moderator

}

object ShowInformation{
  implicit val ShowInformationWrites = Json.writes[ShowInformation]
  implicit val ShowInformationReads = Json.reads[ShowInformation]
}