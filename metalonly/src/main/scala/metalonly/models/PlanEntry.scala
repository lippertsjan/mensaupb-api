package metalonly.models

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads, _}

case class PlanEntry(start: Date,
                     end: Date,
                     showInformation: ShowInformation)

object PlanEntry {
    import play.api.libs.json.Writes.dateWrites

    implicit val customDateWrites: Writes[java.util.Date] = dateWrites("yyyy-MM-dd'T'HH:mm")

    implicit val PlanEntryWrites: Writes[PlanEntry] = Json.writes[PlanEntry]

    //noinspection ConvertibleToMethodValue because of overload
    implicit val PlanEntryReads: Reads[PlanEntry] = (
      (JsPath \ "day").read[String] and
        (JsPath \ "time").read[String] and
        (JsPath \ "duration").read[Int] and
        (JsPath \ "moderator").read[String] and
        (JsPath \ "show").read[String] and
        (JsPath \ "genre").read[String]
      ) (PlanEntry.apply(_, _, _, _, _, _))

    def apply(day: String,
              time: String,
              duration: Int,
              moderator: String,
              show: String,
              genre: String): PlanEntry = {
      val readPattern = "dd'.'MM'.'yy'T'HH':'mm"

      val cal = Calendar.getInstance

      val dateTimeAsString = s"${day}T$time"
      val startDate = new SimpleDateFormat(readPattern).parse(dateTimeAsString)

      cal.setTime(startDate)
      cal.add(Calendar.HOUR, duration)
      val endDate = cal.getTime

      PlanEntry(startDate, endDate, ShowInformation(moderator, show, genre))
  }

}