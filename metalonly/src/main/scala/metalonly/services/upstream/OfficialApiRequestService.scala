package metalonly.services.upstream

import com.google.inject.Inject
import play.api.cache.AsyncCacheApi
import play.api.libs.ws.{WSClient, WSRequest}
import play.api.{Configuration, Logger}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class OfficialApiRequestService  @Inject()(ws: WSClient,
                                           cache: AsyncCacheApi,
                                           config: Configuration)
                                          (implicit exec: ExecutionContext){

  def requestStats() = doRequest("stats")

  def requestPlan() = doRequest("plannew")

  def requestAll() = doRequest("all")

  private def doRequest(action: String) = {
    val extRequest: WSRequest = ws.url("https://metal-only.de/botcon/mob.php")

    val baseParameterMap = Map("action" -> action, "outputformat" -> "json")


    val complexRequest: WSRequest =
      extRequest.withHttpHeaders("Accept" -> "application/json")
        .withRequestTimeout(10000.millis)
        .withQueryStringParameters(baseParameterMap.toSeq: _*)

    Logger.debug(s"Request: $complexRequest")


    complexRequest.get()
  }
}
