package metalonly.services

import com.google.inject.Inject
import metalonly.models.PlanWrapper
import metalonly.services.upstream.OfficialApiRequestService
import play.api.Configuration
import play.api.cache.AsyncCacheApi
import play.api.libs.json.{JsError, JsSuccess}
import play.api.libs.ws.WSResponse

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class PlanService @Inject()(cache: AsyncCacheApi,
                            remoteRequestService: OfficialApiRequestService,
                            config: Configuration)
                           (implicit exec: ExecutionContext) {

  val CACHE_DURATION_IN_SECONDS = 300.seconds

  def getPlan() = getDeprecatedPlan().map(_.plan)

  def getDeprecatedPlan() = {
    val cacheKey = s"metalonly.plan"

    cache.getOrElseUpdate[PlanWrapper](cacheKey, CACHE_DURATION_IN_SECONDS) {
      remoteRequestService
        .requestPlan()
        .map(parseRawStatsResponse)
    }
  }

  private def parseRawStatsResponse(response: WSResponse) = {
    response
      .json
      .validate[PlanWrapper] match {
      case s: JsSuccess[PlanWrapper] =>
        s.get
      case e: JsError =>
        throw new Exception("Json Errors: " + e.errors.mkString(", "))
    }
  }
}
