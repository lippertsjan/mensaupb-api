package metalonly.services

import com.google.inject.Inject

import scala.concurrent.ExecutionContext

class ModService @Inject()(planService: PlanService)
                          (implicit exec: ExecutionContext) {

  def getMods() =
    planService.getPlan()
      .map { entries =>
        entries
          .map(_.showInformation.moderator)
          .distinct
          .sorted
      }

}
