package metalonly.services

import com.google.inject.Inject
import metalonly.models.Wish
import play.api.Logger
import play.api.libs.ws.WSClient

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class WishService @Inject()(ws: WSClient)
                           (implicit exec: ExecutionContext) {
  private val remoteUrl = "https://www.metal-only.de/wunschgruss.html?do=save"


  val successReponseBodyContentPart = "Wunsch/Gruss wurde erfolgreich übermittelt"

  val errorOnlyModeratedTime = "Wünsche und Grüsse sind nur in der moderierten Sendezeit möglich!"

  val errorPartsMissing = "Fehler: Bitte Wunsch/Gruss und einen Nick angeben"

  def submit(w: Wish, remoteAddress: String): Future[Either[String, String]]
  = {
    val wishAsMap = w.toMap
    val wishAsMapWithClientIp = wishAsMap + ("clientIp" -> remoteAddress)
    ws.url(remoteUrl)
      .withRequestTimeout(10.seconds)
      .post(wishAsMapWithClientIp)
      .map { response =>
        val body = response.body
        val result = body match {
          case s if s.contains(successReponseBodyContentPart) => Left(successReponseBodyContentPart)
          case e if e.contains(errorOnlyModeratedTime) => Right(errorOnlyModeratedTime)
          case e2 if e2.contains(errorPartsMissing) => Right(errorPartsMissing)
          case _ => Right("Unbekannter Fehler")
        }
        Logger.debug(s"Wish request $w from $remoteAddress resulted in '$result'.")
        result
      }
  }

}
