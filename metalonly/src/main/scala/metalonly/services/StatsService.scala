package metalonly.services

import com.google.inject.Inject
import metalonly.models.Stats
import metalonly.services.upstream.OfficialApiRequestService
import play.api.Configuration
import play.api.cache.AsyncCacheApi
import play.api.libs.json.{JsError, JsSuccess}
import play.api.libs.ws.WSResponse

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class StatsService @Inject()(cache: AsyncCacheApi,
                             remoteRequestService: OfficialApiRequestService,
                             config: Configuration)
                            (implicit exec: ExecutionContext) {

  val CACHE_DURATION = 30.seconds

  def getStats() = {
    val cacheKey = s"metalonly.stats"

    cache.getOrElseUpdate[Stats](cacheKey, CACHE_DURATION) {
      remoteRequestService
        .requestStats()
        .map(parseRawStatsResponse)
    }
  }


  private def parseRawStatsResponse(response: WSResponse) = {
    response
      .json
      .validate[Stats] match {
      case s: JsSuccess[Stats] =>
        s.get
      case e: JsError =>
        throw new Exception("Json Errors: " + e.errors.mkString(", "))
    }
  }
}
