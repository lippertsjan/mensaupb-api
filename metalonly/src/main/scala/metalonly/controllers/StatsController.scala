package metalonly.controllers

import com.google.inject.Inject
import metalonly.services.StatsService
import play.api.Configuration
import play.api.cache.AsyncCacheApi
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, AnyContent, InjectedController}
import utils.Log

import scala.concurrent.ExecutionContext

class StatsController @Inject()(ws: WSClient,
                                statsService: StatsService,
                                cache: AsyncCacheApi,
                                config: Configuration)
                               (implicit exec: ExecutionContext)
  extends InjectedController {

  def stats() : Action[AnyContent] = Action.async {
    statsService.getStats().map { stats =>
      Ok(Json.toJson(stats))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }
  def showinformation(): Action[AnyContent] = Action.async {
    statsService.getStats().map { stats =>
      Ok(Json.toJson(stats.showInformation))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }

  def track(): Action[AnyContent] = Action.async {
    statsService.getStats().map { stats =>
      Ok(Json.toJson(stats.track))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }
}
