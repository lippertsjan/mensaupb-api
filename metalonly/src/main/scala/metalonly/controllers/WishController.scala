package metalonly.controllers

import com.google.inject.Inject
import metalonly.models.Wish
import metalonly.services.WishService
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.{Action, AnyContent, InjectedController, Request}
import utils.Log

import scala.concurrent.{ExecutionContext, Future}

class WishController @Inject()(wishService: WishService)
                              (implicit exec: ExecutionContext)
  extends InjectedController {
  def submit(): Action[AnyContent] = Action.async { request =>
    request.body.asJson.map { json =>
      json.validate[Wish].map {
        case w: Wish => {
          val clientIp = getClientIp(request)
          val wishResult = wishService.submit(w, clientIp)
          wishResult.map {
            case Left(s) => {
              Logger.debug(s"Wish $w from ${clientIp} sent.")
              Ok(Json.toJson(s))
            }
            case Right(e) => {
              Log.hideAndLog(new Exception("error: " + e))
              BadRequest(Json.toJson(e))
            }
            case _ => InternalServerError(Json.toJson("Unknown error"))
          }
        }
      }.recoverTotal {
        e => Future.successful(BadRequest("Detected error:" + JsError.toFlatForm(e)))
      }
    }.getOrElse {
      Future.successful(BadRequest("Expecting Json data"))
    }
  }

  private def getClientIp(request: Request[AnyContent]) = {
    val xForwardedForValue = request.headers.toMap.getOrElse("X-Forwarded-For", Seq(""))

    // Fallback on heroku router ip if xforwarded is empty. More info see https://devcenter.heroku.com/articles/http-routing#heroku-headers
    xForwardedForValue
      .headOption
      .getOrElse(request.remoteAddress)
  }
}