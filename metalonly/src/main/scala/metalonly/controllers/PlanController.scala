package metalonly.controllers

import com.google.inject.Inject
import metalonly.services.PlanService
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, InjectedController}
import utils.Log

import scala.concurrent.ExecutionContext

class PlanController @Inject()(planService: PlanService)
                              (implicit exec: ExecutionContext)
  extends InjectedController {

  def deprecatedPlan(): Action[AnyContent] = Action.async {
    planService.getDeprecatedPlan().map { plan =>
      Ok(Json.toJson(plan))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }

  def plan(): Action[AnyContent] = Action.async {
    planService.getPlan().map { entries =>
      Ok(Json.toJson(entries))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }
}
