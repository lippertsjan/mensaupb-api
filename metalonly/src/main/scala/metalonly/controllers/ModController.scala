package metalonly.controllers

import com.google.inject.Inject
import metalonly.services.{ModService, StatsService}
import play.api.Configuration
import play.api.cache.AsyncCacheApi
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, AnyContent, InjectedController}
import utils.Log

import scala.concurrent.ExecutionContext

class ModController @Inject()(ws: WSClient,
                              modService: ModService,
                              cache: AsyncCacheApi,
                              config: Configuration)
                             (implicit exec: ExecutionContext)
  extends InjectedController {

  def getMods() : Action[AnyContent] = Action.async {
    modService.getMods().map { mods =>
      Ok(Json.toJson(mods))
    }.recover { case e =>
      Log.hideAndLog(e)
      InternalServerError
    }
  }
}
